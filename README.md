Election System 
===============================

Framework : 
-------------------------
Yii Framework

Description
-------------------------
Use to manage and  monitor election programme

Requirement
-------------------------
1 - PHP 5.4^

2 - Database : MySQL

Setup
-------------------------
1 - Clone

2 - Open cmd/terminal, and change directory to current this folder directory

3 - (todo) On cmd/terminal, run 
```
yii migrate
```
4 - There is sql dump file inside this folder. Execute inside MySql Database with 'campaign_db' as  Database name.

Todo :
-------------------------
1 - Update migration method

