<?php

class WebUser extends CWebUser {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
//    public function checkAccess($operation, $params = array()) {
//        if (empty($this->id)) {
//
//            return false;
//        }
//        $role = $this->getState("role");
//        if ($role === 'admin') {
//            return true; // admin role has access to everything
////            return $this->getName()=='admin1';
//        } 
////        else if ($role === 'superadmin') {
////            return $this->getName()=='superadmin';
////        }
//        // allow access if the operation request is the current user's role
//        return ($operation === $role);
//    }


    function isSuperAdmin() {
        $role = $this->getState("role");
        if ($role === 'superadmin') {
            return true;
        }
        return false;
    }

    function isCandidate() {
        $role = $this->getState("role");
        if ($role === 'candidate') {
            return true;
        }
        return false;
    }

    function isVoter() {
        $role = $this->getState("role");
        if ($role === 'voter') {
            return true;
        }
        return false;
    }

    function voteFlag() {
        
        $voteFlag = $this->getState("voteFlag");
//        $voteFlag =  Account::model()->findByAttributes(array('username' => Yii::app()->user->name));
//        $this->setState('voteFlag', $voteFlag->voteFlag);
        if ($voteFlag === '1') {    
            return true;
        }
        
        return false;
    }
    function setFlag() {
//        $voteFlag =  Account::model()->findByPk($id);
//        $voteFlag = Yii::app()->user->voteFlag + 1;
        $id = Yii::app()->user->id;
        $records =  Account::model()->findByPk($id);
//        $records->voteFlag = $records->voteFlag + 1;
        $this->setState('voteFlag', $records->voteFlag);
//        $this->setState('voteFlag', $voteFlag->voteFlag);
    }

}
