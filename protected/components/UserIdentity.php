<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private $id, $role;

    public function authenticate() {
        $record = Account::model()->findByAttributes(array('NoMatrik' => strtoupper($this->username)));
        //$blackList = MppList::model()->findByAttributes(array('username' => $this->username));

        if ($record === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (strtoupper($record->NoIC) !== /* md5($this->password) */ $this->password)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->id = $record->ID;
            $this->setState('voteFlag', $record->voteFlag);
            $this->setState('role', $record->role);
            $this->setState('dep', $record->department);
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->id;
    }

//
//    public function getRole() {
//        return $this->role;
//    }
//    public function setFlag($id) {
//        $records = Account::model()->findByPk($id);
//        $records->voteFlag = $record->voteFlag+1;
//        $this->setState('voteFlag', $record->voteFlag);
//        return $this->voteFlag;
//    }
}
