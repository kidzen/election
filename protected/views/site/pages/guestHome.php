<div class="guestUser">
    <?php
    $this->widget('ext.bcountdown.BCountdown', array(
        'title' => 'Countdown', // Title
        'message' => 'to campaign day', // message for user
        'isDark' => false, // two skin dark and light , by default it light i.e isDark=false
        'year' => '2015',
        'month' => '11',
        'day' => '4',
        'hour' => '24',
        'min' => '0',
        'sec' => '0',
            )
    );
    ?>
    <p><?php echo CHtml::button('Login To Vote', array('submit' => array('site/login'))); ?></p>

</div>  