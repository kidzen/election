<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>
<div style="text-align: center">
     <!--style="text-align: center"-->
    <h1>Welcome <i><?php echo CHtml::encode(Yii::app()->user->name); ?>!</i></h1>

    <?php if (!Yii::app()->user->isGuest) { ?>
        <div class="authenticateUser">
            <?php //$this->renderPartial('pages/guestHome'); ?>
        </div>

        <?php if (Yii::app()->user->isSuperAdmin()) { ?>
            <?php $this->renderPartial('pages/superadminHome'); ?>

        <?php } else if (Yii::app()->user->isVoter()) { ?>
            <?php $this->renderPartial('pages/voterHome'); ?>

        <?php } ?>
    <?php } else { ?>
        <?php $this->renderPartial('pages/guestHome'); ?>

    <?php } ?>

</div>
