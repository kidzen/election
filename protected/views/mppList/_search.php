<?php
/* @var $this MppListController */
/* @var $model MppList */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation'=>true,
        'enableClientValidation'=>true,
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'focus'=>array($model,'Name'),
    ));
    ?>

    <div class="row">
        <?php echo $form->label($model, 'ID'); ?>
        <?php echo $form->textField($model, 'ID',['class'=>'left']); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Department Id'); ?>
        <?php echo $form->textField($model,'DepId',array('size'=>5,'maxlength'=>5)); ?>
        <?php //
//        echo $form->dropDownList($model, 'DepId', array(
//            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
//            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
//            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
        ?>
    </div>
    <?php if (Yii::app()->user->isSuperAdmin()) { ?>
        <div class="row">
            <?php echo $form->label($model, 'Image Path'); ?>
            <?php echo $form->textField($model, 'imgPath', array('size' => 60, 'maxlength' => 200)); ?>
        </div>

        <div class="row">
            <?php echo $form->label($model, 'Logo Path'); ?>
            <?php echo $form->textField($model, 'logoPath', array('size' => 60, 'maxlength' => 200)); ?>
        </div>
    <?php } ?>
    <div class="row">
        <?php echo $form->label($model, 'Name'); ?>
        <?php echo $form->textField($model, 'Name', array('size' => 60, 'maxlength' => 200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Department'); ?>
        <?php echo $form->textField($model,'Department',array('size'=>60,'maxlength'=>200)); ?>
        <?php
//        echo $form->dropDownList($model, 'Department', array(
//            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
//            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
//            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
        ?>

    </div>

    <div class="row">
        <?php echo $form->label($model, 'Session'); ?>
        <?php echo $form->textField($model, 'Session', array('size' => 60, 'maxlength' => 200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Vote Count'); ?>
        <?php echo $form->textField($model, 'voteCount'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Candidate'); ?>
        <?php echo $form->textField($model,'candidateFor',array('size'=>5,'maxlength'=>5));  ?>
        <?php
//        echo $form->dropDownList($model, 'candidateFor', array(
//            'PBC' => 'PBC', 'WIN' => 'WIN', 'NON' => 'NON',
//            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
//            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
//            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM'));
        ?>

    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
