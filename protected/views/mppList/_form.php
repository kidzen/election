<?php
/* @var $this MppListController */
/* @var $model MppList */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'mpp-list-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'Department Id'); ?>
        <?php // echo $form->textField($model,'DepId',array('size'=>5,'maxlength'=>5)); ?>
        <?php
        echo $form->dropDownList($model, 'DepId', array(
            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
        ?>
        <?php echo $form->error($model, 'DepId'); ?>
    </div>
    <?php if (Yii::app()->user->isSuperAdmin()) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'Image Path'); ?>
            <?php echo $form->fileField($model, 'imgPath'); ?>
            <?php echo $form->error($model, 'imgPath'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'Logo Path'); ?>
            <?php echo $form->fileField($model, 'logoPath'); ?>
            <?php echo $form->error($model, 'logoPath'); ?>
        </div>
    <?php } ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'Name'); ?>
        <?php echo $form->textField($model, 'Name', array('size' => 60, 'maxlength' => 200)); ?>
        <?php echo $form->error($model, 'Name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Department'); ?>
        <?php echo $form->textField($model,'Department',array('size'=>60,'maxlength'=>200)); ?>
        <?php
//        echo $form->dropDownList($model, 'Department', array(
//            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
//            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
//            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
        ?>
        <?php echo $form->error($model, 'Department'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Session'); ?>
        <?php echo $form->textField($model, 'Session', array('size' => 60, 'maxlength' => 200)); ?>
        <?php echo $form->error($model, 'Session'); ?>
    </div>
    <?php if (Yii::app()->user->isSuperAdmin()) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'Vote Count'); ?>
            <?php echo $form->textField($model, 'voteCount'); ?>
            <?php echo $form->error($model, 'voteCount'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'Candidate'); ?>
            <?php // echo $form->textField($model,'candidateFor',array('size'=>5,'maxlength'=>5)); ?>
            <?php
            echo $form->dropDownList($model, 'candidateFor', array(
                'PBC' => 'PBC', 'WIN' => 'WIN', 'NON' => 'NON',
                'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
                'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
                'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM'));
            ?>
            <?php echo $form->error($model, 'candidateFor'); ?>

        </div>
    <?php } ?>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
