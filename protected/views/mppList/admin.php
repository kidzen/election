<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs = array(
    'Mpp Lists' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List MppList', 'url' => array('index')),
    array('label' => 'Create MppList', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mpp-list-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Manage Mpp Lists</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'mpp-list-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'ID' => array(
            'name' => 'ID',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'DepId' => array(
            'name' => 'DepId',
            'header' => 'Department Id',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'Picture' => array(
            'header' => 'Picture',
            'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
            'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:80px;height:auto;"))',
        ),
        'Logo' => array(
            'header' => 'Logo',
            'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
            'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->logoPath,"",array("style"=>"width:80px;height:auto;"))',
        ),
        'Name' => array(
            'name' => 'Name',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'Department' => array(
            'name' => 'Department',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'Session' => array(
            'name' => 'Session',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'voteCount' => array(
            'name' => 'voteCount',
            'header' => 'Vote Count',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        'candidateFor' => array(
            'name' => 'candidateFor',
            'header' => 'Candidate',
            'htmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
