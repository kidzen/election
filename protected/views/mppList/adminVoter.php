<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs = array(
    'Mpp Lists' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List MppList', 'url' => array('admin2')),
);
?>

<h1>Manage Mpp Lists</h1>
<h4 style="text-decoration-color: red">Please revise your choice before voting as your choices are irreversible! </h4> 
<?php
if (Yii::app()->user->voteFlag == 0) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'mpp-list-grid',
        'dataProvider' => $modelDepartment->searchDepartment(),
//        'filter'=>$modelDepartment,
        'enablePagination' => false, //not show full data
        'columns' => array(
            'ID' => array(
                'name' => 'ID',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'DepId' => array(
                'name' => 'DepId',
                'header' => 'Department Id',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Picture' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:80px;height:auto;"))',
            ),
            'Logo' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->logoPath,"",array("style"=>"width:80px;height:auto;"))',
            ),
            'Name' => array(
                'name' => 'Name',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Department' => array(
                'name' => 'Department',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Session' => array(
                'name' => 'Session',
                'htmlOptions' => array('style' => 'text-align:center;'),
				
				
            ), /*
            'voteCount' => array(
                'name' => 'voteCount',
                'header' => 'Vote Count',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ), */
			
			
            'candidateFor' => array(
                'name' => 'candidateFor',
                'header' => 'Candidate',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Vote' => array(
                'class' => 'CButtonColumn',
                'template' => '{vote}',
                'buttons' => array
                    (
                    'vote' => array
                        (
                        'label' => 'vote', //if (Yii::app()->user->voteFlag() === 1){ return 'vote'} else { return 'Thanks for voting for me!'},
                        'url' => 'Yii::app()->controller->createUrl("updateVoter",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'ajax' => array(
                                'confirm' => 'Yes or No',
                                'type' => 'get',
                                'url' => 'js:$(this).attr("href")',
                                'success' => 'js:function(data) { $.fn.yiiGridView.update("mpp-list-grid")'),
//                        'readOnly' => TRUE //Yii::app()->user->voteFlag()
                        ),
//                    
                    ),
                ),
            ),
        ),
    ));
} else if (Yii::app()->user->voteFlag == 1) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'mpp-list-grid',
        'dataProvider' => $modelDepartment->searchPublic(),
        'enablePagination' => false, //not show full data
        'columns' => array(
            'ID' => array(
                'name' => 'ID',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'DepId' => array(
                'name' => 'DepId',
                'header' => 'Department Id',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Picture' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:80px;height:auto;"))',
            ),
            'Logo' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->logoPath,"",array("style"=>"width:80px;height:auto;"))',
            ),
            'Name' => array(
                'name' => 'Name',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Department' => array(
                'name' => 'Department',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Session' => array(
                'name' => 'Session',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),/*
            'voteCount' => array(
                'name' => 'voteCount',
                'header' => 'Vote Count',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),*/
			
            'candidateFor' => array(
                'name' => 'candidateFor',
                'header' => 'Candidate',
                'htmlOptions' => array('style' => 'text-align:center;'),
            ),
            'Vote' => array(
                'class' => 'CButtonColumn',
                'template' => '{vote}',
                'buttons' => array
                    (
                    'vote' => array
                        (
                        'label' => 'vote', //if (Yii::app()->user->voteFlag() === 1){ return 'vote'} else { return 'Thanks for voting for me!'},
                        'url' => 'Yii::app()->controller->createUrl("updateVoter",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'ajax' => array(
                                'confirm' => 'Yes or No',
                                'type' => 'get',
                                'url' => 'js:$(this).attr("href")',
                                'success' => 'js:function(data) { $.fn.yiiGridView.update("mpp-list-grid")'),
//                        'readOnly' => TRUE //Yii::app()->user->voteFlag()
                        ),
//                    
                    ),
                ),
            ),
        ),
    ));
} else {
    echo '<h3>Thanks for voting..!</h3>';
}
?>





