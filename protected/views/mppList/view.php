<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs=array(
	'Mpp Lists'=>array('index'),
	$model->Name,
);

$this->menu=array(
	array('label'=>'List MppList', 'url'=>array('index')),
	array('label'=>'Create MppList', 'url'=>array('create'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Update MppList', 'url'=>array('update', 'id'=>$model->ID), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Delete MppList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Manage MppList', 'url'=>array('admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
);
?>

<h1>View MppList #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'DepId',
		'imgPath',
		'logoPath',
		'Name',
		'Department',
		'Session',
		'voteCount',
		'candidateFor',
	),
)); ?>
