<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="en">

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <style>
            /*.outer{
                background-color: thistle;
            }*/

            .bg{
                padding:0px;
                /*height: 100%; */
				/*background-color:transparent;*/
            }
        </style>
    </head>


    <body  class="outer">

        <div class="container" id="page">

            <div id="header" style="padding:0px">
                <div id="logo" class="bg" style="padding:0px 0px">                      <!-- BANNER -->
                    <?php  CHtml::image(Yii::app()->baseUrl . "/images/undi/web_4x10-picom.PNG","",array("style"=>"width:100%;height:250px;")); ?>
                    <?php  echo CHtml::image(Yii::app()->baseUrl . "/images/election.jpg","",array("style"=>"width:100%;")); ?>
                </div>


            </div> <!-- header -->

            <div id="mainmenu" style="padding-top:0px">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                'items' => array(
                array('label' => 'Home', 'url' => array('/site/index')),
                array('label' => 'Vote', 'url' => array('/mppList/adminVoter'), 'visible' => Yii::app()->user->isVoter()),
                array('label' => 'Manage MPP', 'url' => array('/mppList/admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
                array('label' => 'Manage User Account', 'url' => array('/account/admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
                array('label' => 'Manage Account', 'url' => array('/account/' . Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                ),
                ));
                ?>
            </div><!-- mainmenu
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> Created by <?= Yii::app()->params['creator'] ?>.<br/><!-- ni footer tu...ejas la...kalau xnk buang-->
                All Rights Reserved.<br/>

            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
