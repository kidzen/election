<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs=array(
	'Mpp Lists'=>array('index'),
	$model->Name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List MppList', 'url'=>array('index')),
	array('label'=>'Create MppList', 'url'=>array('create')),
	array('label'=>'View MppList', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage MppList', 'url'=>array('admin')),
);
?>

<h1>Update MppList <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>