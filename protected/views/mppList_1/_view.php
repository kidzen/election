<?php
/* @var $this MppListController */
/* @var $data MppList */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DepId')); ?>:</b>
	<?php echo CHtml::encode($data->DepId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imgPath')); ?>:</b>
	<?php echo CHtml::encode($data->imgPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logoPath')); ?>:</b>
	<?php echo CHtml::encode($data->logoPath); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Department')); ?>:</b>
	<?php echo CHtml::encode($data->Department); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Session')); ?>:</b>
	<?php echo CHtml::encode($data->Session); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('voteCount')); ?>:</b>
	<?php echo CHtml::encode($data->voteCount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('candidateFor')); ?>:</b>
	<?php echo CHtml::encode($data->candidateFor); ?>
	<br />

	*/ ?>

</div>