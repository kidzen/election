<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs=array(
	'Mpp Lists'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MppList', 'url'=>array('index')),
	array('label'=>'Create MppList', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#mpp-list-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Mpp Lists</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mpp-list-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'ID',
		'DepId',
		'imgPath',
		'logoPath',
		'Name',
		'Department',
		/*
		'Session',
		'voteCount',
		'candidateFor',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); 
////////backup

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'mpp-list-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
//    'enablePagination' => false, //not show full data
    'columns' => array(
        'ID',
        'DepId' => array(
            'name' => 'DepId',
            'header' => 'Department Id',
            'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
        ),
        'Picture' => array(
            'header' => 'Picture',
            'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
            'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:80px;height:auto;"))',
        ),
        'Name' => array(
            'name' => 'Name',
            'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
        ),
        'Department' => array(
            'name' => 'Department',
            'header' => 'Department',
            'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
        ),
        'Session' => array(
            'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            'name' => 'Session',
//            'Deprt' => array(
//                'header'=>'Department',
//                'type'=>'html',
//                'value'=>'$data->Department' .','. '$data->Session',
        ),
        'Candidate' => array(
            'name' => 'candidateFor',
            'header' => 'Candidate',
//                'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
            'visible' => true,
        ),
        'Logo' => array(
            'header' => 'Logo',
            'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
            'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->logoPath,"",array("style"=>"width:80px;height:auto;"))',
        ),
        'Vote Count' => array(
            'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            'value' => '$data->voteCount',
//            'voteCount',
        ),
        'Vote' => array(
            'class' => 'CButtonColumn',
            'template' => '{vote}',
            'buttons' => array
                (
                'vote' => array
                    (
                    'label' => 'vote', //if (Yii::app()->user->voteFlag() === 1){ return 'vote'} else { return 'Thanks for voting for me!'},
                    'url' => 'Yii::app()->controller->createUrl("update2",array("id"=>$data->primaryKey))',
                    'options' => array(
                        'ajax' => array(
                            'confirm' => 'Yes or No',
                            'type' => 'get',
                            'url' => 'js:$(this).attr("href")',
                            'success' => 'js:function(data) { $.fn.yiiGridView.update("mpp-list-grid")'),
//                        'readOnly' => TRUE //Yii::app()->user->voteFlag()
                    ),
//                    
                ),
            ),
        ),
    ),
));

