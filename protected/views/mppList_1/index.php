<?php
/* @var $this MppListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mpp Lists',
);

$this->menu=array(
	array('label'=>'Create MppList', 'url'=>array('create'), 'visible'=>  Yii::app()->user->isSuperAdmin()),
	array('label'=>'Manage MppList', 'url'=>array('admin'), 'visible'=>  Yii::app()->user->isSuperAdmin()),
	array('label'=>'Vote', 'url'=>array('adminVoter'), 'visible'=>  Yii::app()->user->isVoter()),
);
?>

<h1>Mpp Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
