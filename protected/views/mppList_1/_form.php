<?php
/* @var $this MppListController */
/* @var $model MppList */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mpp-list-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'DepId'); ?>
		<?php echo $form->textField($model,'DepId',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'DepId'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imgPath'); ?>
		<?php echo $form->textField($model,'imgPath',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'imgPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logoPath'); ?>
		<?php echo $form->textField($model,'logoPath',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'logoPath'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Name'); ?>
		<?php echo $form->textField($model,'Name',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Department'); ?>
		<?php // echo $form->textField($model,'Department',array('size'=>60,'maxlength'=>200)); ?>
                <?php echo $form->dropDownList($model, 'Department', array('PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'NON' => 'NON'));?>
		<?php echo $form->error($model,'Department'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Session'); ?>
		<?php echo $form->textField($model,'Session',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'Session'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'voteCount'); ?>
		<?php echo $form->textField($model,'voteCount'); ?>
		<?php echo $form->error($model,'voteCount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'candidateFor'); ?>
		<?php echo $form->textField($model,'candidateFor',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'candidateFor'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->