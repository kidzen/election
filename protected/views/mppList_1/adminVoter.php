<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs = array(
    'Mpp Lists' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List MppList', 'url' => array('index')),
);
?>

<h1>Manage Mpp Lists</h1>
<!--<img src="../../../images/undi/diploma fisioterapi/april 14/fisio april 14 (2).jpg" alt=""/>-->

<?php // echo strtoupper(Yii::app()->user->name); ?>
<?php
if (Yii::app()->user->voteFlag == 0) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'mpp-list-grid',
        'dataProvider' => $modelDepartment->search(),
        'enablePagination' => false, //not show full data
        'columns' => array(
            'ID',
            'DepId' => array(
                'name' => 'DepId',
                'header' => 'Department Id',
                'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
            ),
            'Candidate'=>array(
                'name' => 'candidateFor',
                'header' => 'Candidate',
//                'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
                'visible'=>false,
            ),
            'Picture' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:200px;height:auto;"))',
            ),
            'Name' => array(
                'name' => 'Name',
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            ),
            'Department' => array(
                'name' => 'Department',
                'header' => 'Department',
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            ),
            'Session' => array(
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
                'name' => 'Session',
//            'Deprt' => array(
//                'header'=>'Department',
//                'type'=>'html',
//                'value'=>'$data->Department' .','. '$data->Session',
            ), /*
            'Vote Count' => array(
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
                'value' => '$data->voteCount',
//            'voteCount',
            ),*/
			
            'Vote' => array(
                'class' => 'CButtonColumn',
                'template' => '{vote}',
                'buttons' => array
                    (
                    'vote' => array
                        (
                        'label' => 'vote', //if (Yii::app()->user->voteFlag() === 1){ return 'vote'} else { return 'Thanks for voting for me!'},
                        'url' => 'Yii::app()->controller->createUrl("update2",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'ajax' => array(
                                'confirm' => 'Yes or No',
                                'type' => 'get',
                                'url' => 'js:$(this).attr("href")',
                                'success' => 'js:function(data) { $.fn.yiiGridView.update("mpp-list-grid")'),
//                        'readOnly' => TRUE //Yii::app()->user->voteFlag()
                        ),
//                    
                    ),
                ),
            ),
        ),
    ));
} else if (Yii::app()->user->voteFlag == 1) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'mpp-list-grid',
        'dataProvider' => $modelDepartment->search(),
        'enablePagination' => false, //not show full data
        'columns' => array(
            'ID',
            'DepId' => array(
                'name' => 'DepId',
                'header' => 'Department Id',
                'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
            ),
            'Candidate'=>array(
                'name' => 'candidateFor',
                'header' => 'Candidate',
//                'htmlOptions' => array('width' => '50', 'style' => 'text-align:center;'),
                'visible'=>false,
            ),
            'Picture' => array(
                'header' => 'Picture',
                'type' => 'raw',
//            'htmlOptions' => array('width'=>'300px'),
                'value' => 'CHtml::image(Yii::app()->baseUrl . "/images/undi/" . $data->imgPath,"",array("style"=>"width:200px;height:auto;"))',
            ),
            'Name' => array(
                'name' => 'Name',
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            ),
            'Department' => array(
                'name' => 'Department',
                'header' => 'Department',
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
            ),
            'Session' => array(
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
                'name' => 'Session',
//            'Deprt' => array(
//                'header'=>'Department',
//                'type'=>'html',
//                'value'=>'$data->Department' .','. '$data->Session',
            ), /*
            'Vote Count' => array(
                'htmlOptions' => array('width' => '100', 'style' => 'text-align:center;'),
                'value' => '$data->voteCount',
//            'voteCount',

            ),*/
			
            'Vote' => array(
                'class' => 'CButtonColumn',
                'template' => '{vote}',
                'buttons' => array
                    (
                    'vote' => array
                        (
                        'label' => 'vote', //if (Yii::app()->user->voteFlag() === 1){ return 'vote'} else { return 'Thanks for voting for me!'},
                        'url' => 'Yii::app()->controller->createUrl("update2",array("id"=>$data->primaryKey))',
                        'options' => array(
                            'ajax' => array(
                                'confirm' => 'Yes or No',
                                'type' => 'get',
                                'url' => 'js:$(this).attr("href")',
                                'success' => 'js:function(data) { $.fn.yiiGridView.update("mpp-list-grid")'),
//                        'readOnly' => TRUE //Yii::app()->user->voteFlag()
                        ),
//                    
                    ),
                ),
            ),
        ),
    ));    
} else {
    echo 'Thanks for voting';
}
?>





