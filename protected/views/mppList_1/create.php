<?php
/* @var $this MppListController */
/* @var $model MppList */

$this->breadcrumbs=array(
	'Mpp Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MppList', 'url'=>array('index')),
	array('label'=>'Manage MppList', 'url'=>array('admin')),
);
?>

<h1>Create MppList</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>