<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'account-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'No Matrik'); ?>
        <?php echo $form->textField($model, 'NoMatrik', array('size' => 50, 'maxlength' => 50)); ?>
        <?php echo $form->error($model, 'NoMatrik'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'No IC'); ?>
        <?php echo $form->textField($model, 'NoIC', array('size' => 60, 'maxlength' => 200)); ?>
        <?php echo $form->error($model, 'NoIC'); ?>
    </div>
    <p class="hint">
        Hint: You must login with a valid Matrix No.
    </p>
    <?php if (Yii::app()->user->isSuperAdmin()) { ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'Role'); ?>
            <?php // echo $form->textField($model, 'role', array('size' => 20, 'maxlength' => 20)); ?>
            <?php echo $form->dropDownList($model, 'role', array('superadmin' => 'superadmin', 'voter' => 'voter')); ?>
            <?php echo $form->error($model, 'role'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'Vote Flag'); ?>
            <?php // echo $form->textField($model, 'voteFlag'); ?>
            <?php echo $form->dropDownList($model, 'voteFlag', array('0' => '0', '1' => '1', '2' => '2')); ?>
            <?php echo $form->error($model, 'voteFlag'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'Department'); ?>
            <?php // echo $form->textField($model, 'department', array('size' => 5, 'maxlength' => 5)); ?>
            <?php
            echo $form->dropDownList($model, 'department', array(
                'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
                'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
                'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
            ?>
            <?php echo $form->error($model, 'department'); ?>
        </div>
        <?php
    } else {
        
    }
    ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->