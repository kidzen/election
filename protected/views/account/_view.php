<?php
/* @var $this AccountController */
/* @var $data Account */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoMatrik')); ?>:</b>
	<?php echo CHtml::encode($data->NoMatrik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NoIC')); ?>:</b>
	<?php echo CHtml::encode($data->NoIC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
	<?php echo CHtml::encode($data->role); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('voteFlag')); ?>:</b>
	<?php echo CHtml::encode($data->voteFlag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department')); ?>:</b>
	<?php echo CHtml::encode($data->department); ?>
	<br />


</div>