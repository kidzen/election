<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	'Create',
);
if (Yii::app()->user->isSuperAdmin()){
$this->menu=array(
	array('label'=>'List Account', 'url'=>array('index'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Manage Account', 'url'=>array('admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
);}
?>

<h1>Create Account</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>