<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs = array(
    'Accounts' => array('index'),
    $model->ID => array('view', 'id' => $model->ID),
    'Update',
);

$this->menu = array(
    array('label' => 'List Account', 'url' => array('index'), 'visible' => Yii::app()->user->isSuperAdmin()),
    array('label' => 'Create Account', 'url' => array('create'), 'visible' => Yii::app()->user->isSuperAdmin()),
    array('label' => 'View Account', 'url' => array('view', 'id' => $model->ID)),
    array('label' => 'Manage Account', 'url' => array('admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
);
?>

<h1>Update Account <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>