<?php
/* @var $this AccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accounts',
);
if (Yii::app()->user->isSuperAdmin()){
$this->menu=array(
	array('label'=>'Create Account', 'url'=>array('create'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Manage Account', 'url'=>array('admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
);
}
?>

<h1>Accounts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
