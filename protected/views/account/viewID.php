<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Account', 'url'=>array('index'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Create Account', 'url'=>array('create'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Update Account', 'url'=>array('update', 'id'=>$model->ID), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Update Account', 'url'=>array('updateID')),
	array('label'=>'Delete Account', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?'), 'visible' => Yii::app()->user->isSuperAdmin()),
	array('label'=>'Manage Account', 'url'=>array('admin'), 'visible' => Yii::app()->user->isSuperAdmin()),
);
?>

<h1>View Account #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'NoMatrik',
		'NoIC',
		'role',
		'voteFlag',
		'department',
	),
)); ?>
