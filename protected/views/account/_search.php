<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    ));
    ?>

    <div class="row">
        <?php echo $form->label($model, 'ID'); ?>
        <?php echo $form->textField($model, 'ID'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'No Matrik'); ?>
        <?php echo $form->textField($model, 'NoMatrik', array('size' => 50, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'No IC'); ?>
        <?php echo $form->textField($model, 'NoIC', array('size' => 60, 'maxlength' => 200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Role'); ?>
        <?php // echo $form->textField($model, 'role', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->dropDownList($model, 'role', array('superadmin' => 'superadmin', 'voter' => 'voter')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Vote Flag'); ?>
        <?php echo $form->textField($model, 'voteFlag'); ?>
        <?php echo $form->dropDownList($model, 'voteFlag', array('0' => '0', '1' => '1', '2' => '2')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'Department'); ?>
        <?php echo $form->textField($model, 'department', array('size' => 5, 'maxlength' => 5)); ?>
        <?php
//        echo $form->dropDownList($model, 'department', array(
//            'PH' => 'PH', 'NG' => 'NG', 'MA' => 'MA', 'EH' => 'EH',
//            'MS' => 'MS', 'OSH' => 'OSH', 'HOM' => 'HOM', 'PC' => 'PC',
//            'MLT' => 'MLT', 'OT' => 'OT', 'HM' => 'HM', 'NON' => 'NON'));
        ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->