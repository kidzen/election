<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $ID
 * @property string $NoMatrik
 * @property string $NoIC
 * @property string $role
 * @property integer $voteFlag
 * @property string $department
 */
class Account extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'account';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('NoMatrik, NoIC, role, voteFlag', 'required'),
            array('voteFlag', 'numerical', 'integerOnly' => true),
            array('NoMatrik, NoIC', 'unique',
                'message' => 'You have already registered. '
                . 'Please refer to management if you forgot your credentials.'),
            array('NoMatrik', 'length', 'max' => 50),
            array('NoIC', 'length', 'max' => 200),
            array('role', 'length', 'max' => 20),
            array('department', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID, NoMatrik, NoIC, role, voteFlag, department', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID' => 'ID',
            'NoMatrik' => 'No Matrik',
            'NoIC' => 'No Ic',
            'role' => 'Role',
            'voteFlag' => 'Vote Flag',
            'department' => 'Department',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('NoMatrik', $this->NoMatrik, true);
        $criteria->compare('NoIC', $this->NoIC, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('voteFlag', $this->voteFlag);
        $criteria->compare('department', $this->department, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchPersonal() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', Yii::app()->user->id);
        $criteria->compare('NoMatrik', $this->NoMatrik, true);
        $criteria->compare('NoIC', $this->NoIC, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('voteFlag', $this->voteFlag);
        $criteria->compare('department', $this->department, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Account the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public static function checkMatric($model) {
        $matricNo = $model->NoMatrik;

        if (preg_match('/\bEH\d+/', $matricNo)) {
            $model->department = 'EH';
            $model->voteFlag = '1';
        } else if (preg_match('/\bMA\d+/', $matricNo)) {
            $model->department = 'MA';
            $model->voteFlag = '1';
        } else if (preg_match('/\bMS\d+/', $matricNo)) {
            $model->department = 'MS';
            $model->voteFlag = '0';
        } else if (preg_match('/\bHOM\d+/', $matricNo)) {
            $model->department = 'HOM';
            $model->voteFlag = '1';
        } else if (preg_match('/\bMLT\d+/', $matricNo)) {
            $model->department = 'MLT';
            $model->voteFlag = '1';
        } else if (preg_match('/\bNG\d+/', $matricNo)) {
            $model->department = 'NG';
            $model->voteFlag = '0';
        } else if (preg_match('/\bOSH\d+/', $matricNo)) {
            $model->department = 'OSH';
            $model->voteFlag = '1';
        } else if (preg_match('/\bOT\d+/', $matricNo)) {
            $model->department = 'OT';
            $model->voteFlag = '1';
        } else if (preg_match('/\bPC\d+/', $matricNo)) {
            $model->department = 'PC';
            $model->voteFlag = '1';
        } else if (preg_match('/\bHM\d+/', $matricNo)) {
            $model->department = 'HM';
            $model->voteFlag = '1';
        } else if (preg_match('/\bPH\d+/', $matricNo)) {
            $model->department = 'PH';
            $model->voteFlag = '0';
        } else {
            //throw error
            return false;
        }
        return true;

    }

}
