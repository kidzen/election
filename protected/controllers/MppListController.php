<?php

class MppListController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
                'actions' => array('adminView', 'admin2'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions' => array('create', 'update'),
                'actions' => array('indexID', 'viewID', 'admin', 'index'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('adminVoter', 'updateVoter'),
                'expression' => 'Yii::app()->user->isVoter()',
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete'),
                'users' => array('superadmin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionViewID() {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new MppList;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        // var_dump(Yii::app()->basePath.'\\images\\');die();
        if (isset($_POST['MppList'])) {
            $model->attributes = $_POST['MppList'];
            $imgPath=CUploadedFile::getInstance($model,'imgPath');
            $logoPath=CUploadedFile::getInstance($model,'logoPath');

            $imgPathName=time().'_img_'.$imgPath;
            $logoPathName=time().'_logo_'.$logoPath;

            $model->imgPath=$imgPathName;
            $model->logoPath=$logoPathName;
            if ($model->save()){
                $imgPath->saveAs('C:/wamp64/www/_project/election/images/undi/'.$imgPathName);
                $logoPath->saveAs('C:/wamp64/www/_project/election/images/undi/'.$logoPathName);
                $this->redirect(array('view', 'id' => $model->ID));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['MppList'])) {
            $model->attributes = $_POST['MppList'];

            $imgPath=CUploadedFile::getInstance($model,'imgPath');
            $logoPath=CUploadedFile::getInstance($model,'logoPath');

            $imgPathName=time().'_img_'.$imgPath;
            $logoPathName=time().'_logo_'.$logoPath;

            $model->imgPath=$imgPathName;
            $model->logoPath=$logoPathName;
            if ($model->save()){
                $imgPath->saveAs('C:/wamp64/www/_project/election/images/undi/'.$imgPathName);
                $logoPath->saveAs('C:/wamp64/www/_project/election/images/undi/'.$logoPathName);
                $this->redirect(array('view', 'id' => $model->ID));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('MppList');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new MppList('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MppList']))
            $model->attributes = $_GET['MppList'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAdminIndex() {
        $model = new MppList('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MppList']))
            $model->attributes = $_GET['MppList'];

        $this->renderPartial('admin', array(
            'model' => $model,
        ));
    }

    public function actionAdmin2() {
        $model = new MppList('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['MppList']))
            $model->attributes = $_GET['MppList'];

        $this->render('admin2', array(
            'model' => $model,
        ));
    }

    public function actionAdminView() {
        $modelDepartment = new MppList('search');
        $modelDepartment->unsetAttributes();  // clear any default values

        $modelPublic = new MppList('search');
        $modelPublic->unsetAttributes();  // clear any default values

        $modelWin = new MppList('search');
        $modelWin->unsetAttributes();  // clear any default values
//        $modelPublic->candidateFor = 'PBC';
//        $modelDepartment->candidateFor = '<>PBC';
//        $modelWin->candidateFor = 'WIN';

        if (isset($_GET['MppList'])) {
            $model->attributes = $_GET['MppList'];
            $modelPublic->attributes = $_GET['MppList'];
            $modelDepartment->attributes = $_GET['MppList'];
            $modelWin->attributes = $_GET['MppList'];
        }
//        $this->render
        $this->render('adminView', array(
//            'model' => $model,
            'modelDepartment' => $modelDepartment,
            'modelPublic' => $modelPublic,
            'modelWin' => $modelWin,
        ));
    }

    public function actionAdminVoter() {
        $modelDepartment = new MppList('search');
        $modelDepartment->unsetAttributes();  // clear any default values

        $modelPublic = new MppList('search');
        $modelPublic->unsetAttributes();  // clear any default values
//        $flag = Yii::app()->user->voteFlag();
        if (isset($_GET['MppList'])) {
            $modelPublic->attributes = $_GET['MppList'];
            $modelDepartment->attributes = $_GET['MppList'];
//            $modelDepartment->candidateFor = Yii::app()->user->dep;
        }
        $this->render('adminVoter', array(
            'modelDepartment' => $modelDepartment,
            'modelPublic' => $modelPublic,
        ));
    }

//            if (preg_match('/EH.*/', strtoupper(Yii::app()->user->name))) {
////            $model->attributes = $_GET['MppList'];
//                $modelDepartment->candidateFor = 'EH';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/MA.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'MA';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/MS.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'MS';
////                $modelPublic->candidateFor = 'PBC';
////            $this->render('adminMS', array(
////            'modelDepartment' => $modelDepartment,
////            'modelPublic' => $modelPublic,
//////            'test' => $arra,
////        ));
//            } else if (preg_match('/HOM.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'HOM';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/MLT.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'MLT';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/NG.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'NG';
////                $modelPublic->candidateFor = 'PBC';
////            $this->render('adminNG', array(
////            'modelDepartment' => $modelDepartment,
////            'modelPublic' => $modelPublic,
//////            'test' => $arra,
////        ));
//            } else if (preg_match('/OSH.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'OSH';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/OT.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'OT';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/PC.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'PC';
////                $modelPublic->candidateFor = 'PBC';
//            } else if (preg_match('/PH.*/', strtoupper(Yii::app()->user->name))) {
//                $modelDepartment->candidateFor = 'PH';
////                $modelPublic->candidateFor = 'PBC';
////            $this->render('adminPH', array(
////            'modelDepartment' => $modelDepartment,
////            'modelPublic' => $modelPublic,
//////            'test' => $arra,
////        ));
//            } else {
//                $modelDepartment->candidateFor = 'NON';
//                $modelPublic->candidateFor = 'NON';
//            }
//        } else {
////            throw new CHttpException(404, 'The requested page does not exist.');
////            throw new CDbException('The requested data cant be fetch.');
//        }

    public function actionUpdateVoter($id) {
        if (Yii::app()->user->setFlag() < 2) {
//            $model = $this->loadModel($id);
            $model = MppList::model()->findByPk($id);
            $model->voteCount = $model->voteCount + 1;
            $model->save(false);
            $userID = Yii::app()->user->id;

            $modelUser = Account::model()->findByPk($userID);
            $modelUser->voteFlag = $modelUser->voteFlag + 1;
            $modelUser->save(false);
            Yii::app()->user->setFlag();
        }
        $this->redirect(array('mppList/adminVoter'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MppList the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = MppList::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MppList $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mpp-list-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
