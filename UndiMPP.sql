/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17 : Database - campaign_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`campaign_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `campaign_db`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `NoMatrik` varchar(50) NOT NULL,
  `NoIC` varchar(200) NOT NULL,
  `role` varchar(20) NOT NULL,
  `voteFlag` tinyint(1) NOT NULL,
  `department` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `account` */

insert  into `account`(`ID`,`NoMatrik`,`NoIC`,`role`,`voteFlag`,`department`) values (1,'superadmin','superadmin','superadmin',0,'NON'),(2,'EH23','EH23','voter',0,'EH'),(3,'EH11','EH11','voter',0,'EH'),(4,'EH22','EH22','voter',0,'EH'),(5,'PH22','PH22','voter',2,'PH'),(6,'MS23','MS23','voter',2,'MS'),(7,'NG23','891123035572','voter',0,'NG'),(8,'NG22','NG22','voter',2,'NG'),(9,'NG11','NG11','voter',0,'NG'),(10,'NG2','NG2','voter',0,'NG'),(11,'NG3','NG3','voter',0,'NG');

/*Table structure for table `mpp_list` */

DROP TABLE IF EXISTS `mpp_list`;

CREATE TABLE `mpp_list` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `DepId` varchar(5) NOT NULL,
  `imgPath` varchar(200) NOT NULL,
  `logoPath` varchar(200) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Department` varchar(200) NOT NULL,
  `Session` varchar(200) NOT NULL,
  `voteCount` int(10) NOT NULL,
  `candidateFor` varchar(5) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `mpp_list` */

insert  into `mpp_list`(`ID`,`DepId`,`imgPath`,`logoPath`,`Name`,`Department`,`Session`,`voteCount`,`candidateFor`) values (1,'PH','diploma fisioterapi/april 14/fisio april 14 (2).jpg','diploma fisioterapi/april 14/fisio logo (2).jpg','Normaidayu Bt Mohd Zainal','Diploma Fisioterapi','April 2014',1,'PH'),(2,'PH','diploma fisioterapi/april 14/fisioterapi april 14.jpg','diploma fisioterapi/april 14/logo.jpg','Nor Hayati Bt Mohammad Chik','Diploma Fisioterapi','April 2014',0,'PH'),(3,'PH','diploma fisioterapi/april 15/fisioterapi april 15.jpg','diploma fisioterapi/april 15/logo.jpg','Muhammad Faris Bin Megat Abdul Halim','Diploma Fisioterapi','April 2015',0,'PH'),(4,'PC','menang tanpa bertanding/diploma farmasi julai14/diploma farmasi julai 14.jpg','menang tanpa bertanding/diploma farmasi julai14/logo.jpg','Muhammad Aiman Akmal Bin Sahrol','Diploma Farmasi','Julai 2014',0,'WIN'),(5,'NG','diploma sains kejururawatan/julai 14/sains kejururawatan julai 14(2).jpg','diploma sains kejururawatan/julai 14/logo(2).jpg','Muhammad Syafiq Bin Othman','Diploma Sains Kejururawatan','Julai 2014',1,'NG'),(6,'NG','diploma sains kejururawatan/julai 14/sains kejururawatan julai 14.jpg','diploma sains kejururawatan/julai 14/logo.jpg','Iman Athika Bt Laili','Diploma Sains Kejururawatan','Julai 2014',0,'NG'),(7,'MS','diploma sains perubatan/dec 14/sains perubatan dec 14.jpg','diploma sains perubatan/dec 14/logo.jpg','Nor Syahidah Binti Tayurman','Diploma Sains Perubatan','Disember 2014',1,'MS'),(8,'MS','diploma sains perubatan/julai 14/sains perubatan julai 14.jpg','diploma sains perubatan/julai 14/logo.jpg','Mohd Nazrul Axham Bin Rosdi','Diploma Sains Perubatan','Julai 2014',0,'MS'),(9,'PH','umum/diploma fisio april 14/umum april 14.jpg','umum/diploma fisio april 14/logo.jpg','Muhammad Haziq Bin Mohd Kamal','Diploma Fisioterapi','April 2014',1,'PBC'),(10,'PH','umum/diploma fisio april 15/umum april 15.jpg','umum/diploma fisio april 15/logo.jpg','Hamize Shah Bin Akbarsah','Diploma Fisioterapi','April 2015',0,'PBC'),(11,'PC','umum/farmasi julai 14/umum julai 14.jpg','umum/farmasi julai 14/logo.jpg','Nur Syafiqah Binti Muhammad Zul Hisham','Diploma Farmasi','Julai 2014',0,'PBC'),(12,'PC','umum/farmasi julai 14(2)/umum julai 14.jpg','umum/farmasi julai 14(2)/logo.jpg','Amalina Zayanah Binti Ramli','Diploma Farmasi','Julai 2014',0,'PBC'),(13,'OSH','umum/kesihatan & keslamtan prkrjaan julai 14/umum julai 14.jpg','umum/kesihatan & keslamtan prkrjaan julai 14/logo.jpg','Muhammad Ridhwan Bin Rosli','Diploma Kesihatan & Keselamatan Pekerjaan ','Julai 2014',1,'PBC'),(14,'EH','umum/kesihatan persekitaran julai 14/umum julai4.jpg','umum/kesihatan persekitaran julai 14/logo.jpg','Siti Nur Syafiqah Binti Razif','Diploma Kesihatan Persekitaran ','Julai 2014',0,'PBC'),(15,'MA','umum/pmbantu prubatan julai 14/umum julai 14.jpg','umum/pmbantu prubatan julai 14/logo.jpg','Muhammad Zaaimuddin Bin Mat Zuber','Diploma Pembantu Perubatan ','Julai 2014',1,'PBC'),(16,'NG','umum/sains kejururawatan april 14/umum april 14.jpg','umum/sains kejururawatan april 14/logo.jpg','Nur Hazwani Binti Ibrahim','Diploma Sains Kejururawatan','April 2014',0,'PBC'),(17,'NG','umum/sains kejururawatan julai 14/umum julai 14.jpg','umum/sains kejururawatan julai 14/logo.jpg','Alif Haikal B. Mohd Azhar','Diploma Sains Kejururawatan','Julai 2014',0,'PBC'),(18,'MS','umum/sains perubatan julai 14/sains prubatan julai 14.jpg','umum/sains perubatan julai 14/logo.jpg','Nurmahan Farahin Binti Farhad','Diploma Sains Perubatan','Julai 2014',0,'PBC'),(19,'MLT','umum/tknologi mkml perubatan dis 14/umum dis 14.jpg','umum/tknologi mkml perubatan dis 14/logo.jpg','Nurul Najiha Athira Bte Abu Sa\'amah','Diploma Teknologi Makmal Perubatan ','Disember 2014',0,'PBC'),(20,'OSH','menang tanpa bertanding/kesihatan & keselamatan perkerjaan/kesihatan & keselamatan pekerjaan april 14.jpg','menang tanpa bertanding/kesihatan & keselamatan perkerjaan/logo.jpg','Abdul Muiz Bin Abdul Munaaim','Diploma Kesihatan & Keselamatan Pekerjaan ','April 2014',0,'WIN'),(21,'EH','menang tanpa bertanding/kesihatan persekitaran/kesihatan persekitaran april 14.jpg','menang tanpa bertanding/kesihatan persekitaran/logo.jpg','Siti Mastura Binti Isa','Diploma Kesihatan Persekitaran ','April 2014',0,'WIN'),(22,'MA','menang tanpa bertanding/pembantu perubatan/pembantu perubatan julai 14.jpg','menang tanpa bertanding/pembantu perubatan/logo.jpg','Siti Nur Syamimi Bt Zainol Abidin','Diploma Pembantu Perubatan ','Julai 2014',0,'WIN'),(23,'MLT','menang tanpa bertanding/teknologi makmal perubatan/teknologi makmal perubatan dec 14.jpg','menang tanpa bertanding/teknologi makmal perubatan/logo.jpg','Nurul Najiha Athira Bte Abu Sa\'amah','Diploma Teknologi Makmal Perubatan ','Disember 2014',0,'WIN'),(24,'OT','menang tanpa bertanding/terapi cara kerja/terapi cara kerja julai 14.jpg','menang tanpa bertanding/terapi cara kerja/logo.jpg','Rabiatul Adawiyah Bt Hj Utoh Said','Diploma Terapi Cara Kerja','Julai 2014',0,'WIN');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
